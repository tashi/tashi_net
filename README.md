# TashiNet

tashi_net is the networking extension of the [TASHI](https://gitlab.gwdg.de/jhoelke/tashi/) test framework. 
It adds support for SSH, SCP, FTP, Mail, ...  

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'tashi_net'
```

If you run tashi on windows under `JRuby` you might need to add `jruby-pageant` to your gemfile as-well. 
And then execute:

    $ bundle install

Finally require the gem in your tashi extension. 

```ruby
# lib/tashi/tashi.rb
require 'tashi_net'
```

## Versioning

We follow the [Semantic Versioning](https://semver.org/) pattern. Major versions of this gem are always aligned & compatible with TASHI major versions.
You should specify major and minor version in the Gemfile, e.g. `gem 'tashi_net', '~> X.Y'`. 

## Usage

See example test cases in the [TASHI Example Project](https://gitlab.gwdg.de/jhoelke/tashi-example) 
or have a look at the [API Documentation](https://jhoelke.pages.gwdg.de/tashi_net/doc/).

## Development & Contributing

Contributions are very welcome. Fork the project and issue a pull request. See also 
[TASHI Contribution Guide](https://gitlab.gwdg.de/jhoelke/tashi/-/blob/master/CONTRIBUTING.md) for further details.

## Code of Conduct

This project is intended to be a safe, welcoming space for collaboration.
Everyone interacting in the TashiNet project's codebases, issue trackers, chat rooms and/or mailing lists 
is expected to follow the [code of conduct](CODE_OF_CONDUCT.md).

## License

Project is licensed under [Apache 2.0](LICENSE) License.