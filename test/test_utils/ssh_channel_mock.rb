# frozen_string_literal: true

class SshChannelMock < Minitest::Mock
  attr_reader :command

  def initialize(delegator = nil)
    super
    @called = {}
    @command = Ssh::Command.new('UnitTest')
  end

  def synchronized
    record_call(:synchronized)
    yield
  end

  def called(name)
    @called.fetch(name, 0)
  end

  private

  def record_call(name)
    @called[name] ||= 0
    @called[name] += 1
  end
end
