# frozen_string_literal: true

require 'test_helper'

# Tests for Ssh::Sender helper class
# rubocop:disable Metrics/MethodLength
class SshSenderTest < Tashi::UnitTest
  def setup
    super
    @channel = Minitest::Mock.new
    @callbacks = Minitest::Mock.new
    @sender = Ssh::Sender.new(@channel, @callbacks)
  end

  def teardown
    super
    @callbacks.verify
    @channel.verify
  end

  def test_close
    @channel.expect :close, :called
    assert_equal(:called, @sender.close)
  end

  def test_local_closed
    @channel.expect :local_closed?, true
    assert(@sender.local_closed?)
  end

  def test_inactive
    @channel.expect :active?, true
    @channel.expect :closing?, true
    assert(@sender.inactive?)

    @channel.expect :active?, false
    assert(@sender.inactive?)

    @channel.expect :active?, true
    @channel.expect :closing?, false
    assert(!@sender.inactive?)
  end

  def test_execution
    @channel.expect :on_data, nil
    @channel.expect :on_extended_data, nil
    @channel.expect :on_process, nil
    @channel.expect :on_request, nil, ['exit-status']
    @channel.expect :on_request, nil, ['exit-signal']
    @channel.expect :request_pty, nil
    @channel.expect :exec, nil, ['test cmd']

    cmd = Minitest::Mock.new
    cmd.expect :instruction, 'test cmd'
    cmd.expect :instruction, 'test cmd'

    @sender.exec(cmd, pty: true)

    cmd.verify
  end

  def test_enter
    filter = Minitest::Mock.new
    filter.expect :filter, 'filtered_out', %w[foo out]
    @channel.expect :send_data, nil, ["foo\n"]
    @callbacks.expect :wait_for_condition, %w[out err], [['bar']]
    @callbacks.expect :filter, filter

    assert_equal(%w[filtered_out err], @sender.enter('foo', ['bar']))

    filter.verify
  end
end
# rubocop:enable Metrics/MethodLength
