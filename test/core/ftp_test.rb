# frozen_string_literal: true

require 'test_helper'

# Tests for Ssh helper class
# rubocop:disable Metrics/ClassLength
class FtpTest < Tashi::UnitTest
  class FtpTestImpl < Ftp
    attr_accessor :ftp
  end

  def setup
    super
    @host = Host.new(name: 'local', fqdn: 'localhost')
    @session = Minitest::Mock.new
  end

  def teardown
    super
    @session.verify
    Tashi::ThreadRegistry.clear(:ftp_sessions)
  end

  def test_with_session
    @session.expect :connect, @session
    @session.expect :test_call, nil, ['foo']
    @session.expect :close, nil

    Ftp.stub(:new, @session) do
      Ftp.with_session(@host) do |ftp|
        ftp.test_call('foo')
      end
    end
  end

  # Ensure the sessions for this thread are closed when calling close hook
  # but not sessions from other threads (i.e. other parallel pools)
  def test_close_hook
    ftp_own_thread = Minitest::Mock.new
    ftp_own_thread.expect :close, nil

    Tashi::ThreadRegistry.register(ftp_sessions: ftp_own_thread)

    Ftp.close_hook
    ftp_own_thread.verify
  end

  def test_connection_error
    ftp = Ftp.new(@host)
    Net::FTP.stub(:new, -> { raise 'Tis is totally unexpected' }) do
      assert_raises { ftp.connect }
    end
  end

  def test_connection
    @session.expect :passive=, nil, [false]
    @session.expect :open_timeout=, nil, [Integer]
    @session.expect :read_timeout=, nil, [Integer]
    @session.expect :debug_mode=, nil, [false]
    @session.expect :connect, nil, [@host.address, Integer]
    @session.expect :login, nil, [String, Object]

    @session.expect :nil?, false
    @session.expect :closed?, false

    ftp = Ftp.new(@host)
    Net::FTP.stub(:new, @session) { ftp.connect }
  end

  def test_closing
    ftp = FtpTestImpl.new(@host)

    # nothing should happen, session is not open
    ftp.close

    ftp.ftp = @session
    @session.expect :nil?, false
    @session.expect :nil?, false
    @session.expect :closed?, false
    @session.expect :closed?, true
    @session.expect :close, nil
    ftp.close
  end

  def test_list
    ftp = FtpTestImpl.new(@host)
    ftp.ftp = @session
    @session.expect :nil?, false
    @session.expect :nil?, false

    # Session is open. should work
    @session.expect :closed?, false
    @session.expect :nlst, %w[. .. bar.txt], ['/foo']
    assert_equal(%w[. .. bar.txt], ftp.list('/foo'))

    # Session is closed. should raise
    @session.expect :closed?, true
    assert_raises(IllegalStateError) { ftp.list('/foo') }
  end

  def test_download
    ftp = FtpTestImpl.new(@host)
    ftp.ftp = @session
    @session.expect :nil?, false
    @session.expect :closed?, false
    @session.expect :getbinaryfile, :ok, %w[foo.txt /tmp/bar.txt]
    assert_equal(:ok, ftp.download('foo.txt', '/tmp/bar.txt'))
  end

  def test_upload
    ftp = FtpTestImpl.new(@host)
    ftp.ftp = @session
    @session.expect :nil?, false
    @session.expect :closed?, false
    @session.expect :putbinaryfile, :ok, %w[/tmp/bar.txt foo.txt]
    assert_equal(:ok, ftp.upload('/tmp/bar.txt', 'foo.txt'))
  end

  def test_rename
    ftp = FtpTestImpl.new(@host)
    ftp.ftp = @session
    @session.expect :nil?, false
    @session.expect :closed?, false
    @session.expect :rename, :ok, %w[old new]
    assert_equal(:ok, ftp.rename('old', 'new'))
  end

  def test_delete
    ftp = FtpTestImpl.new(@host)
    ftp.ftp = @session
    @session.expect :nil?, false
    @session.expect :closed?, false
    @session.expect :delete, :ok, %w[file/name.txt]
    assert_equal(:ok, ftp.delete('file/name.txt'))
  end

  def test_to_s
    ftp = FtpTestImpl.new(@host)
    ftp.ftp = @session
    @session.expect :nil?, false
    @session.expect :closed?, false
    assert_equal('FTP on ftp://localhost:21 is connected: true', ftp.to_s)

    @session.expect :nil?, false
    @session.expect :closed?, true
    assert_equal('FTP on ftp://localhost:21 is connected: false', ftp.to_s)
  end
end
# rubocop:enable Metrics/ClassLength
