# frozen_string_literal: true

require 'test_helper'

class TashiNetTest < Tashi::UnitTest
  def test_it_has_a_version
    refute_nil ::TashiNet::VERSION
  end
end
