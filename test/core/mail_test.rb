# frozen_string_literal: true

require 'test_helper'

# Tests for Ssh helper class
class MailTest < Tashi::UnitTest
  def setup
    super
    @session = Minitest::Mock.new
    Net::POP3.stub(:new, @session) do
      @mail = Mail.new('test.mail', 'tester', 'test123')
    end
  end

  def teardown
    super
    @session.verify
  end

  def test_account
    assert_equal('tester@test.mail', @mail.account)
  end

  def test_count
    expect_connections(3)

    @session.expect :n_mails, nil
    assert_equal(0, @mail.count)

    @session.expect :n_mails, 0
    assert_equal(0, @mail.count)

    @session.expect :n_mails, 42
    assert_equal(42, @mail.count)
  end

  def test_clear
    expect_connections
    @session.expect :delete_all, nil
    @mail.clear
  end

  def test_wait_success
    logger = InMemoryLog.new
    DepP.global.reregister(:logger) { logger }

    expect_connections(3)
    @session.expect :n_mails, 0
    @session.expect :n_mails, 2
    @session.expect :n_mails, 2
    @mail.wait_for_mails(interval: 0.0)

    logs = logger.info_messages.join("\n")
    assert(logs.include?('Found 2 mails for tester@test.mail'))
  end

  def test_wait_timeout
    expect_connections(1)
    @session.expect :n_mails, 0
    assert_raises(TestFailure) { @mail.wait_for_mails(interval: 0.01, timeout: 0) }
  end

  # rubocop:disable Metrics/MethodLength
  def test_fetch
    mocks = [mail_mock('test mail'), mail_mock('rökdöts are cool!')]
    expect_connections(2)
    @session.expect :n_mails, 2
    @session.expect :mails, mocks

    mails = {
      'test mail' => "From: no@where\r\nSubject: test mail\r\nLorem ipsum dolor set amet.",
      'rökdöts are cool!' => "From: no@where\r\nSubject: rökdöts are cool!\r\nLorem ipsum dolor set amet."
    }
    files = %w[MAIL_r_kd_ts_are_cool_.txt MAIL_test_mail.txt].sort
    assert_equal(mails, @mail.fetch_mails(working_dir: @working_dir))
    assert_equal(files, Dir["#{@working_dir}/*.txt"].map { |e| e.split('/').last }.sort)

    mocks.each(&:verify)
  end
  # rubocop:enable Metrics/MethodLength

  private

  def mail_mock(subject)
    data = "From: no@where\r\nSubject: #{subject}\r\nLorem ipsum dolor set amet."
    mail = Minitest::Mock.new
    mail.expect :pop, data
    mail.expect :delete, data
  end

  def expect_connections(num = 1)
    num.times do
      @session.expect :start, nil, [String, String]
      @session.expect :finish, nil
    end
  end
end
