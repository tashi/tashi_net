# frozen_string_literal: true

require 'test_helper'

# Tests for Ssh::Callbacks helper class
class SshLinuxConverterTest < Tashi::UnitTest
  def test_default
    converter = Ssh::LinuxDataConverter.new
    config = Ssh::LinuxDataConverter::DEFAULT_CONFIG
    replacements = Ssh::LinuxDataConverter::ANSI_REPLACEMENTS
    assert_equal(config, converter.config)
    assert_equal(replacements, converter.replacements)
  end

  def test_no_replacement
    converter = Ssh::LinuxDataConverter.new(filter_ansi: false)
    config = Ssh::LinuxDataConverter::DEFAULT_CONFIG.merge(filter_ansi: false)
    replacements = {}
    assert_equal(config, converter.config)
    assert_equal(replacements, converter.replacements)
  end

  def test_full_config
    converter = Ssh::LinuxDataConverter.new(replace_xterm_escapes: true)
    config = Ssh::LinuxDataConverter::DEFAULT_CONFIG.merge(replace_xterm_escapes: true)
    replacements = Ssh::LinuxDataConverter::ANSI_REPLACEMENTS.merge(Ssh::LinuxDataConverter::XTERM_REPLACEMENTS)
    assert_equal(config, converter.config)
    assert_equal(replacements, converter.replacements)
  end

  def test_replacing
    converter = Ssh::LinuxDataConverter.new
    assert_equal('HelloWorld', converter.convert("\e[31mHelloWorld"))
    assert_equal('test', converter.convert("\e[mtest"))
    assert_equal(123, converter.convert(123))
    assert_equal(false, converter.convert(false))
  end
end
