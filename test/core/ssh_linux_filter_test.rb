# frozen_string_literal: true

require 'test_helper'

# Tests for Ssh::Callbacks helper class
class SshLinuxFilterTest < Tashi::UnitTest
  def setup
    super
    @filter = Ssh::LinuxResponseFilter.new(filter_shell_input_request: true)
  end

  def test_shell_input_prompts
    regexes = @filter.input_requests
    assert(regexes.any? { |r| r.match?('foo@localhost:~$ ') })
    assert(regexes.any? { |r| r.match?('foo@localhost:~# ') })
    assert(regexes.any? { |r| r.match?('foo@localhost:~> ') })
    assert(regexes.all? { |r| !r.match?('enter password: ') })
  end

  def test_filter
    assert_equal('/foo/bar', @filter.filter('pwd', "pwd\n/foo/bar").strip)
    assert_equal('/foo/bar', @filter.filter('pwd', '/foo/bar').strip)
    assert_equal('/foo/bar', @filter.filter('pwd', "/foo/bar\n foo@localhost:~# ").strip)
    assert_equal('/foo/bar', @filter.filter('pwd', "pwd\n/foo/bar\n foo@localhost:~# ").strip)
    assert_equal('test 123 foo', @filter.filter(123, 'test 123 foo').strip)
  end
end
