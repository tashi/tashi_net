# frozen_string_literal: true

require 'test_helper'

# Tests for Scp helper class
class ScptTest < Tashi::UnitTest
  # Test Implementation to inject dependencies and access privat methods
  class TestScp < Scp
    attr_accessor :connector

    # Accessor for private method
    def merger(options)
      ssh_options(options)
    end

    # Accessor for private method
    def reporter(name, sent, total)
      process_reporting(nil, name, sent, total)
    end
  end

  def setup
    super
    @source = '/tmp/test'
    @destination = '/tmp/_test_file'
    @ssh_options = { host_key: 'ssh-dss', user: 'test', password: 'test123' }

    @host = Host.new(name: 'local', fqdn: 'localhost')
    @host.ssh_properties = @ssh_options

    @scp = TestScp.new(@host)
    @scp.connector = Minitest::Mock.new
  end

  def test_options_without_ssh
    assert_equal({ ssh: @host.ssh_properties }, @scp.merger({}))
    assert_equal({ ssh: @host.ssh_properties, verbose: true }, @scp.merger({ verbose: true }))
  end

  def test_options_with_ssh
    options = { ssh: { host_key: 'ssh-rsa' }, recursive: true }
    ssh_options = @host.ssh_properties.dup
    ssh_options[:host_key] = 'ssh-rsa'
    assert_equal({ ssh: ssh_options, recursive: true }, @scp.merger(options))

    options = { ssh: { port: 22 }, recursive: true }
    ssh_options = @host.ssh_properties.dup
    ssh_options[:port] = 22
    assert_equal({ ssh: ssh_options, recursive: true }, @scp.merger(options))
  end

  # rubocop:disable Metrics/MethodLength
  # rubocop:disable Metrics/AbcSize
  def test_progress_logger
    logger = InMemoryLog.new
    DepP.global.reregister(:logger) { logger }

    @scp.reporter('foo', 5.0, 100)
    @scp.reporter('foo', 5.0, 100)
    @scp.reporter('bar', 5.0, 100)
    @scp.reporter('foo', 5.0, 100)
    @scp.reporter('foo', 10.0, 100)
    @scp.reporter('bar', 25, 100)
    @scp.reporter('foo', 11, 100)
    @scp.reporter('foo', 20.0, 100)
    @scp.reporter('foo', 55.0, 100)
    @scp.reporter('bar', 100, 100)
    @scp.reporter('foo', 100, 100)

    assert_equal(1, logger.times_logged("bar: \t 5% of 100"))
    assert_equal(1, logger.times_logged("bar: \t 25% of 100"))
    assert_equal(1, logger.times_logged("foo: \t 5% of 100"))
    assert_equal(1, logger.times_logged("foo: \t 10% of 100"))
    assert_equal(1, logger.times_logged("foo: \t 20% of 100"))
    assert_equal(1, logger.times_logged("foo: \t 55% of 100"))
    assert_equal(1, logger.times_logged("foo: \t 100% of 100 \t \\[Done\\]"))

    assert(logger.never_logged?('foo: 	 11% of 100'))
  end
  # rubocop:enable Metrics/MethodLength
  # rubocop:enable Metrics/AbcSize

  def test_upload
    options = { ssh: { host_key: 'ssh-rsa' }, recursive: true }
    expectations = options.dup
    expectations[:ssh].merge!(@ssh_options)
    @scp.connector.expect(:upload!, nil, [@host.fqdn, @host.user, @source, @destination, expectations])
    @scp.upload(@source, @destination, options: options)
    @scp.connector.verify
  end

  def test_upload_other_user
    @scp.connector.expect(:upload!, nil, [@host.fqdn, 'foo', @source, @destination, { ssh: @ssh_options }])
    @scp.upload(@source, @destination, user: 'foo')
    @scp.connector.verify
  end

  def test_upload_other_ssh_user
    options = { ssh: { user: 'bar' } }
    expectations = { ssh: @ssh_options.merge(options[:ssh]) }
    @scp.connector.expect('upload!', nil, [@host.fqdn, 'bar', @source, @destination, expectations])
    @scp.upload(@source, @destination, options: options)
    @scp.connector.verify
  end

  def test_download
    options = { ssh: { host_key: 'ssh-rsa' }, recursive: true }
    expectations = options.dup
    expectations[:ssh].merge!(@ssh_options)
    @scp.connector.expect(:download!, nil, [@host.fqdn, @host.user, @source, @destination, expectations])
    @scp.download(@source, @destination, options: options)
    @scp.connector.verify
  end

  def test_download_other_user
    @scp.connector.expect(:download!, nil, [@host.fqdn, 'foo', @source, @destination, { ssh: @ssh_options }])
    @scp.download(@source, @destination, user: 'foo')
    @scp.connector.verify
  end

  def test_parallel_download
    # Here we want to use stub for a static class.
    # Thus we cannot use the Mock approach as we do for instances.
    @scp.connector = Net::SCP

    # Mock configuration
    # The download method returns a thread obj.
    thread_mock = Minitest::Mock.new
    thread_mock.expect(:wait, true, [])
    scp_mock = Minitest::Mock.new
    scp_mock.expect(:download, thread_mock, [@source, @destination, @scp.merger({})])

    Net::SCP.stub(:start, [@host.ip, @host.user, @host.ssh_properties], scp_mock) do
      @scp.parallel_download([{ source: @source, destination: @destination }])
    end

    scp_mock.verify
  end
end
