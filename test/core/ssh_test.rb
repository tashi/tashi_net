# frozen_string_literal: true

require 'test_helper'

# Tests for Ssh helper class
# rubocop:disable Metrics/MethodLength
class SshTest < Tashi::UnitTest
  def setup
    super
    @host = Host.new(name: 'local', fqdn: 'localhost')
    @host.ssh_properties = { user: 'test', password: 'test123' }
    @session = Minitest::Mock.new
    @channel = Minitest::Mock.new
  end

  def teardown
    super
    @session.verify
    @channel.verify
    Tashi::ThreadRegistry.clear(:ssh_sessions)
  end

  def test_connect
    ssh = Ssh.new(@host)
    Net::SSH.stub(:start, @session) { ssh.connect }

    @session.expect :nil?, false
    @session.expect :closed?, false
    assert(ssh.open?)
  end

  def test_connect_error
    ssh = Ssh.new(@host)
    Net::SSH.stub(:start, -> { raise 'How Unexpected' }) { assert_raises { ssh.connect } }
  end

  def test_connect_retry
    ssh = Ssh.new(@host)
    @called = 0

    # First raise an error, then return the session mock on second try
    connector = proc {
      @called += 1
      raise 'Errno::EBADF: (Bad file descriptor)' if @called < 2

      return @session
    }

    Net::SSH.stub(:start, connector) do
      ssh.connect # We ignore the first error and use the session on second try.
    end

    @session.expect :nil?, false
    @session.expect :closed?, false
    assert(ssh.open?)
  end

  def test_close
    ssh = Ssh.new(@host)
    ssh.close # should do nothing, as we don't have a session

    Net::SSH.stub(:start, @session) { ssh.connect }
    @session.expect :nil?, false
    @session.expect :closed?, false
    @session.expect :close, false

    # should send close to the session
    ssh.close
  end

  # Ensure the sessions for this thread are closed when calling close hook
  # but not sessions from other threads (i.e. other parallel pools)
  def test_close_hook
    ssh_own_thread = Minitest::Mock.new
    ssh_own_thread.expect :close, nil

    Tashi::ThreadRegistry.register(ssh_sessions: ssh_own_thread)

    Ssh.close_hook
    ssh_own_thread.verify
  end

  def test_invoke
    ssh = Ssh.new(@host)

    @session.expect :nil?, false
    @session.expect :nil?, false
    @session.expect :closed?, false
    @channel.expect :exec, :cmd_result, [@session, Object]
    Net::SSH.stub(:start, @session) { ssh.connect }
    Ssh::Channel.stub(:new, @channel) do
      assert_equal(:cmd_result, ssh.invoke('test cmd'))
    end
  end

  # rubocop:disable Metrics/AbcSize
  def test_with_channel
    ssh = Ssh.new(@host)
    thread = Minitest::Mock.new
    thread.expect :join, nil

    @session.expect :nil?, false
    @session.expect :nil?, false
    @session.expect :closed?, false
    @channel.expect :close, nil
    @channel.expect :exec!, thread, [@session, true, Array]
    @channel.expect :command, :cmd_result
    @channel.expect :test_call, nil, ['foo']

    Net::SSH.stub(:start, @session) { ssh.connect }
    Ssh::Channel.stub(:new, @channel) do
      result =
        ssh.with_channel('test cmd', wait_for: []) do |channel|
          channel.test_call('foo')
        end
      assert_equal(:cmd_result, result)
    end

    thread.verify
  end
  # rubocop:enable Metrics/AbcSize

  def test_with_session
    @session.expect :connect, @session
    @session.expect :test_call, nil, ['foo']
    @session.expect :close, nil

    Ssh.stub(:new, @session) do
      Ssh.with_session(@host) do |ssh|
        ssh.test_call('foo')
      end
    end
  end
end
# rubocop:enable Metrics/MethodLength
