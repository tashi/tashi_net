# frozen_string_literal: true

require 'test_helper'

# Tests for Ssh::Command helper class
class SshCommandTest < Tashi::UnitTest
  def setup
    super
    @cmd = Ssh::Command.new('test')
  end

  def test_succeeded
    @cmd.exit_code = -1
    assert(!@cmd.succeeded?)

    @cmd.exit_code = 1
    assert(!@cmd.succeeded?)

    @cmd.exit_code = 0
    assert(@cmd.succeeded?)
  end

  def test_out
    @cmd.result(%W[Foo\nBar\n \xC5rcsec\nBar])
    assert_equal("Foo\nBar", @cmd.utf8_out)
    assert_equal("?rcsec\nBar", @cmd.utf8_err)
  end

  def test_to_s
    @cmd.std_out = 'foo'
    assert(@cmd.to_s.include?("Command 'test'"))
    assert(@cmd.to_s.include?('STD_OUT'))
    assert(!@cmd.to_s.include?('STD_ERR'))

    @cmd.std_err = 'bar'
    assert(@cmd.to_s.include?('STD_ERR'))
  end
end
