# frozen_string_literal: true

require 'test_helper'

# Tests for extension of the assertion module
class AssertTest < Tashi::UnitTest
  def test_command_success
    Assert::SSH.success(cmd(0))

    assert_raises(TestFailure) { Assert::SSH.success(cmd(-1)) }
    assert_raises(TestFailure) { Assert::SSH.success(cmd(1)) }
    assert_raises(TestFailure) { Assert::SSH.success(cmd(nil)) }
    assert_raises(TestFailure) { Assert::SSH.success(cmd('foo')) }
  end

  private

  def cmd(exit_code)
    cmd = Ssh::Command.new('test')
    cmd.result(%w[ssh_out ssh_err])
    cmd.exit_code = exit_code
    cmd
  end
end
