# frozen_string_literal: true

require 'test_helper'

# Tests for Ssh::Callbacks helper class
# rubocop:disable Metrics/ClassLength
class SshCallbacksTest < Tashi::UnitTest
  # Test implementation to provide access to internals
  class Callbacks < Ssh::Callbacks
    attr_writer :out_wait_condition, :out_wait_expressions

    def statement_match(data)
      signal_wait_statement_match(data)
    end

    def response(data)
      determine_response(data)
    end
  end

  def setup
    super
    @channel = SshChannelMock.new
    @converter = Minitest::Mock.new
    @filter = Minitest::Mock.new
    @callbacks = Callbacks.new(@channel, @converter, @filter)
  end

  def teardown
    super

    @channel.verify
    @converter.verify
    @filter.verify
  end

  def test_on_data
    setup_expectations('this is a result string' => 'converted result')

    @callbacks.on_data('this is a result string')

    assert_equal(1, @channel.called(:synchronized))
    assert_equal('converted result', @channel.command.std_out)
    assert_empty(@channel.command.std_err)
  end

  def test_multiple_data
    setup_expectations(
      'this is a result string' => 'converted result ',
      'this is another result string' => 'converted result 2'
    )

    @callbacks.on_data('this is a result string')
    @callbacks.on_data('this is another result string')

    assert_equal(2, @channel.called(:synchronized))
    assert_equal('converted result converted result 2', @channel.command.std_out)
    assert_empty(@channel.command.std_err)
  end

  def test_on_error
    setup_expectations('this is a result string' => 'converted result')

    @callbacks.on_error('this is a result string')

    assert_equal(1, @channel.called(:synchronized))
    assert_equal('converted result', @channel.command.std_err)
    assert_empty(@channel.command.std_out)
  end

  def test_exit_status
    data = Minitest::Mock.new
    data.expect :read_long, 123
    @callbacks.on_exit_status(data)
    assert_equal(123, @channel.command.exit_code)
    data.verify
  end

  def test_exit_signal
    data = Minitest::Mock.new
    data.expect :read_string, 'test'

    @channel.expect :local_closed?, true
    @callbacks.on_exit_signal(data)
    assert_equal(-1, @channel.command.exit_code)

    @channel.expect :local_closed?, false
    @callbacks.on_exit_signal(data)
    assert_equal(-11, @channel.command.exit_code)

    data.verify
  end

  def test_on_process_timed_out
    @channel.expect :timed_out=, nil, [false]
    @channel.expect :timed_out=, nil, [true]
    @channel.expect :idle_since=, nil, [Time]
    @channel.expect :idle_since, 11.minutes.ago
    @channel.expect :idle_timeout, 10 * 60 # ten minutes
    @channel.expect :idle_timeout, 10 * 60 # ten minutes
    @channel.expect :close, nil, [-10]
    @callbacks.on_process
    assert_equal(-1, @channel.command.exit_code)
  end

  def test_on_process
    @channel.expect :timed_out=, nil, [false]
    @channel.expect :idle_since, 2.minutes.ago
    @channel.expect :idle_timeout, 10 * 60 # ten minutes
    @callbacks.on_process
    assert_equal(-1, @channel.command.exit_code)
  end

  def test_wait_for_empty
    @channel.expect :ready?, false
    @channel.expect :ready?, true
    result = @callbacks.wait_for_condition([])
    assert_equal(['', ''], result)
  end

  def test_wait_for
    @channel.expect :in_out_lock, :lock
    @callbacks.out_wait_condition = Minitest::Mock.new
    @callbacks.out_wait_condition.expect :wait, nil, [:lock]

    result = @callbacks.wait_for_condition(%w[foo bar])

    assert_equal(['', ''], result)
    assert_equal(%w[foo bar], @callbacks.out_wait_expressions)
    @callbacks.out_wait_condition.verify
  end

  def test_statement_match
    @callbacks.out_wait_expressions = [/foo/, /b.r/, /b.z/]
    @callbacks.out_wait_condition = Minitest::Mock.new

    # should not match, no broadcast should happen (unexpected)
    @callbacks.statement_match('this is a test').inspect

    @callbacks.out_wait_condition.expect :broadcast, nil
    @callbacks.statement_match('this is a test with foo bar baz').inspect
    # should match, broadcast should happen (expected)
    @callbacks.out_wait_condition.verify
  end

  def test_response
    @channel.command.data_responses = { /password/ => 'melon!', /murder/ => 110 }

    assert_nil(@callbacks.response('you shall not pass'), 'should not match')
    assert_equal([:enter, 'melon!'], @callbacks.response('Speak the password, friend'))
    assert_equal([:close, 110], @callbacks.response('murder on the dance floor'))
  end

  private

  def setup_expectations(map)
    map.each do |key, value|
      @converter.expect :convert, value, [key]
      @channel.expect :idle_since=, nil, [Time]
    end
  end
end
# rubocop:enable Metrics/ClassLength
