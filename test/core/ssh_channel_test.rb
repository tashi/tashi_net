# frozen_string_literal: true

require 'test_helper'

# Tests for Ssh::Channel helper class
# rubocop:disable Metrics/MethodLength
# rubocop:disable Metrics/ClassLength
class SshChannelTest < Tashi::UnitTest
  class ChannelTestImpl < Ssh::Channel
    attr_accessor :in_out_lock, :sender, :timed_out, :callbacks
  end

  class SessionStub
    def open_channel
      yield :channel
    end

    def loop(_unused); end
  end

  def setup
    super
    @session = Minitest::Mock.new
  end

  def teardown
    super
    @session.verify
  end

  def test_initialization
    Ssh::Channel.new('test', 10, 'linux', {})
    assert_raises(IllegalArgumentError) { Ssh::Channel.new('test', 10, 'windows', {}) }
  end

  # rubocop:disable Metrics/AbcSize
  def test_sender_status
    channel = ChannelTestImpl.new('test', 10, 'linux', {})
    assert(!channel.ready?)
    assert(channel.inactive?)
    assert(!channel.local_closed?)

    channel.sender = Minitest::Mock.new
    channel.sender.expect :nil?, false
    channel.sender.expect :nil?, false
    channel.sender.expect :inactive?, true
    channel.sender.expect :local_closed?, true
    assert(channel.ready?)
    assert(channel.inactive?)
    assert(channel.local_closed?)

    channel.sender.verify
  end
  # rubocop:enable Metrics/AbcSize

  def test_execution
    channel = ChannelTestImpl.new('test', 10, 'linux', {})

    @session.expect :open_channel, nil
    @session.expect :loop, nil, [10]
    channel.exec(@session, false)

    channel.timed_out = true
    @session.expect :open_channel, nil
    @session.expect :loop, nil, [10]
    @session.expect :shutdown!, nil
    channel.exec(@session, false)
  end

  def test_execution_io_error
    @session.expect(:open_channel, nil) { raise IOError, 'UnitTest' }

    channel = ChannelTestImpl.new('test', 10, 'linux', {})
    channel.exec(@session, false)

    assert_equal(-2, channel.command.exit_code)
    assert(channel.command.aborted?)
  end

  # Channel is not ready or command is aborted. We don't care on the error
  def test_execution_error_ignored
    @session.expect(:open_channel, nil) { raise 'UnitTest' }

    channel = ChannelTestImpl.new('test', 10, 'linux', {})
    channel.exec(@session, false)

    assert_equal(-1, channel.command.exit_code)
    assert(!channel.command.aborted?)
  end

  # Channel in 'ready' state, but StandardError observed.
  # Expect different exit code
  def test_execution_unexp_error
    @session.expect(:open_channel, nil) { raise 'UnitTest' }

    channel = ChannelTestImpl.new('test', 10, 'linux', {})
    channel.sender = Minitest::Mock.new
    channel.sender.expect :nil?, false # Set the channel in 'ready' state.
    channel.sender.expect :close, nil  # ensure the sender is closed to prevent resource leaks

    channel.exec(@session, false)

    assert_equal(-3, channel.command.exit_code)
    assert(channel.command.aborted?)
  end

  def test_execution_on_sender
    channel = ChannelTestImpl.new('test', 10, 'linux', {})

    sender = Minitest::Mock.new
    sender.expect :default_wait=, nil, [[]]
    sender.expect :exec, true, [Ssh::Command, pty: true]

    Ssh::Sender.stub(:new, sender) do
      channel.exec(SessionStub.new, true, default_wait: [])
    end

    sender.verify
  end

  # Per default the Linux response filter shall be configured as wait_for
  def test_exec_default_wait
    channel = ChannelTestImpl.new('test', 10, 'linux', {})

    sender = Minitest::Mock.new
    sender.expect :default_wait=, nil, [Ssh::LinuxResponseFilter::SHELL_INPUT_REQUESTS]
    sender.expect :exec, true, [Ssh::Command, pty: true]

    Ssh::Sender.stub(:new, sender) do
      channel.exec(SessionStub.new, true)
    end

    sender.verify
  end

  def test_invoke_on_inactive
    channel = ChannelTestImpl.new('test', 10, 'linux', {})
    result = channel.invoke('call on inactive channel')
    assert_equal(-1, result.exit_code)

    assert_raises(TestFailure) { channel.invoke('call on inactive channel', check_success: true) }
  end

  def test_invoke
    channel = ChannelTestImpl.new('test', 10, 'linux', {})
    channel.sender = Minitest::Mock.new
    expect_input(channel, args: ['test cmd', Array], result: %w[out err])
    expect_input(channel, args: ['echo $?', Array], result: ['42', ''])

    result = channel.invoke('test cmd')
    assert_equal(42, result.exit_code)
    assert_equal('out', result.std_out)
    assert_equal('err', result.std_err)

    channel.sender.verify
  end

  def test_invalid_exit_code
    channel = ChannelTestImpl.new('test', 10, 'linux', {})
    assert_equal(-101, channel.exit_code)

    channel.sender = Minitest::Mock.new
    expect_input(channel, args: ['echo $?', Array], result: ['no exit code', ''])
    assert_equal(-1, channel.exit_code)

    channel.sender.verify
  end

  def test_exec!
    channel = ChannelTestImpl.new('test', 10, 'linux', {})
    channel.callbacks = Minitest::Mock.new
    channel.callbacks.expect :wait_for_condition, nil, [%w[wait for]]

    Thread.stub(:new, proc { :exec_thread }) do
      assert_equal(:exec_thread, channel.exec!(@session, false, %w[wait for]))
    end

    channel.callbacks.verify
  end

  private

  def expect_input(channel, args:, result:)
    channel.sender.expect :nil?, false
    channel.sender.expect :inactive?, false
    channel.sender.expect :enter, result, args
  end
end
# rubocop:enable Metrics/MethodLength
# rubocop:enable Metrics/ClassLength
