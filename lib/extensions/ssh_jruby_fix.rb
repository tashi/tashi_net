# frozen_string_literal: true

# Hotfix / workaround for [jruby-openssl/issues/105](https://github.com/jruby/jruby-openssl/issues/105)
# TODO Remove this once the isse is solved
if RUBY_ENGINE == 'jruby'
  Net::SSH::Transport::Algorithms::ALGORITHMS =
    Net::SSH::Transport::Algorithms::ALGORITHMS.each_with_object({}) do |(option, values), result|
      result[option] = values.reject { |value| value =~ /ecd/ }
    end
  Net::SSH::KnownHosts::SUPPORTED_TYPE = Net::SSH::KnownHosts::SUPPORTED_TYPE.reject { |value| value =~ /ecd/ }
end
