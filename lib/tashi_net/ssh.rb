# frozen_string_literal: true

require 'net/ssh'

require_relative 'ssh/command'
require_relative 'ssh/channel'
require_relative 'ssh/callbacks'
require_relative 'ssh/sender'
require_relative 'ssh/linux_data_converter'
require_relative 'ssh/linux_response_filter'

##
# Wrapper around +Net::SSH+, which is a pure-Ruby implementation of the SSH protocol.
#
# If you open an SSH session in your test procedure the tashi-framework will automatically close it
# after the procedure is finished. Ensure to +#connect+ your new +Ssh+ instances. However, there is
# also a session-scoped method that will connect automatically and close the used session immediately after usage:
#
#    Ssh.with_session(my_host) do |ssh|
#      ssh.invoke('cd ~/foo')
#      ssh.invoke('pwd')
#    end
#
# To interact with an interactive environment (e.g. a db console) use the with_channel method instead
#
#    Ssh.with_channel(my_db_host, 'psql') do |psql|
#      psql.enter('\c my_database')
#      psql.enter('SELECT * FROM my_table;')
#    end
#
# You can always specify expected responses and your pre-defined answers to them.
#
#    ssh.invoke('sudo bash', response: {/password/ => 'P4ssw0rd!'})
#
# Depending on the type of the answer the reaction is as follows:
#   - use {Regex => String} syntax to perform automated interaction, e.g. on password requests
#   - use {Regex => Integer} syntax to force an exit with given code on special observed responses
# You can combine those to whatever your call sequence requires.
#
# Author johannes.hoelken@hawk.de
#
# rubocop:disable Metrics/ClassLength
class Ssh
  # We have a long timeout as some tasks (e.g. installation) may take quite a while to finish.
  # This can be overridden by defining +:timeout+ in ssh_properties of the host.
  DEFAULT_TIMEOUT = 60 * 60

  # Default SSH options: Don't use the ssh-agent and only log on warning level.
  # This internal log level only needs to be increased in case of unexpected connection problems
  DEFAULT_OPTIONS = {
    use_agent: false,
    verbose: :warn,
    port: 22
  }.freeze

  ##
  # Open an SSH session to the given host.
  # Method takes a block with has the session object as argument.
  # Simple usage is
  #
  #    Ssh.with_session(my_host) do |ssh|
  #      ssh.invoke('cd ~/foo')
  #      ssh.invoke('pwd')
  #    end
  #
  # ensures the session is closed after the block.
  #
  # @param host (Host) the host to connect to.
  #
  def self.with_session(host)
    session = Ssh.new(host).connect
    yield session
  ensure
    session&.close
  end

  ##
  # static wrapper for @see with_channel method
  def self.with_channel(cmd, host, critical: false, responses: {}, wait_for: nil)
    command = nil
    Ssh.with_session(host) do |session|
      command =
        session.with_channel(cmd, critical: critical, responses: responses, wait_for: wait_for) do |ssh_channel|
          yield ssh_channel
        end
    end
    command
  end

  ##
  # This close hook will be called after each test procedure to close all remaining
  # SSH sessions that have been opened within the current thread.
  def self.close_hook
    Tashi::ThreadRegistry.call_all(ssh_sessions: :close)
    Tashi::ThreadRegistry.clear(:ssh_sessions)
  end

  attr_reader :host, :ssh_options

  ##
  # Constructor
  # ensure to call +connect+ after initialisation
  # @param host (Host) the host to connect to. If a (String) is given the corresponding
  #    host is fetched from the global host registry.
  # @param options (Hash) An optional ssh-options hash. Defaults are taken from the host object.
  def initialize(host, options: {})
    define_host(host)
    define_options(options)
  end

  ##
  # Open an ssh connection to the host
  def connect
    @retry_count = 0
    open_session
    self
  rescue StandardError => e
    retry if retry_connection?(e)
    Tashi.log.error "Failed to open SSH session for #{host.user}@#{host.name}: [#{e.class.name}] #{e.message}"
    raise e
  end

  ##
  # Close the current session
  def close
    return unless open?

    session.close
    Tashi.log.debug "SSH session for #{host.user}@#{host.address} closed..."
  rescue Net::SSH::Disconnect, Net::SSH::Exception => e
    # occurs in case sshd had been stopped (for example due to reboot)
    Tashi.log.debug "SSH session closure failed due to exception: [#{e.class.name}] #{e.message}."
  end

  # Check if the connection is established already
  def open?
    !session.nil? && !session.closed?
  end

  # Execute a command via ssh channel on the host
  #
  # Data Response options
  #   - use {Regex => String} syntax to perform automated interaction, e.g. on password requests
  #   - use {Regex => Integer} syntax to force an exit with given code on special observed responses
  # You can combine those to whatever your call sequence requires.
  #
  # @param cmd (String) the command to execute
  # @param critical (Boolean) By setting to true you can make a failing command critical to you test step.
  # @param responses (Hash) an optional set of expected responses
  # @return (Ssh::Command)
  def invoke(cmd, critical: false, responses: {})
    Tashi.log.info "Executing on #{host.name}: #{cmd}"
    command = send_to_channel(cmd, responses)
    Assert::SSH.success(command) if critical
    command
  end

  # Opens an interactive channel with the given command.
  #
  # For example this could be used to interact with a database shell
  #   ssh.with_channel('psql') do |psql|
  #     psql.enter('\c my_database')
  #     psql.enter('SELECT * FROM my_table;')
  #   end
  #
  # Data Response options
  #   - use {Regex => String} syntax to perform automated interaction, e.g. on password requests
  #   - use {Regex => Integer} syntax to force an exit with given code on special observed responses
  # You can combine those to whatever your call sequence requires.
  #
  # @param cmd (String) the command to execute
  # @param critical (Boolean) By setting to true you can make a failing command critical to you test step.
  # @param responses (Hash) an optional set of expected responses
  # @return (Ssh::Command)
  def with_channel(cmd, critical: false, responses: {}, wait_for: nil)
    command = on_channel(cmd, responses, wait_for) do |ssh_channel|
      yield ssh_channel
    end
    Assert::SSH.success(command) if critical
    command
  end

  private

  attr_reader :session

  # Send command to channel and get result
  def send_to_channel(cmd, data_responses)
    data_responses = @host.ssh_properties.fetch(:data_responses, {}).merge(data_responses)
    ssh_channel = open_channel(cmd, data_responses)
    return ssh_channel.command if @session.nil?

    ssh_channel.exec(@session, @host.ssh_properties[:pty] || data_responses.present?) unless block_given?
  end

  # Send command to channel and keep it open for preceding block commands
  def on_channel(cmd, data_responses, wait_for)
    ssh_channel = open_channel(cmd, @host.ssh_properties.fetch(:data_responses, {}).merge(data_responses))
    return ssh_channel.command if @session.nil?

    thread = ssh_channel.exec!(@session, true, wait_for)
    yield ssh_channel
    ssh_channel.command
  rescue StandardError => e
    thread&.terminate and raise e
  ensure
    ssh_channel&.close
    thread&.join
  end

  def open_channel(command, expected_response)
    raise IllegalStateError, "SSH connection to #{host.user}@#{host.address} isn't open!" unless open?

    Ssh::Channel.new(
      command,
      @host.ssh_properties.fetch(:timeout, DEFAULT_TIMEOUT),
      @host.ssh_properties.fetch(:target_os, 'linux'),
      expected_response
    )
  end

  def open_session
    return if open?

    @session = Net::SSH.start(@host.address, @host.user, @ssh_options)
    Tashi::ThreadRegistry.register(ssh_sessions: self)
  end

  def define_host(host)
    @host = host.is_a?(String) ? DepP.global.host.find(host) : host
  end

  def define_options(options)
    @ssh_options = DEFAULT_OPTIONS.dup
    @ssh_options[:logger] = Tashi.log('net-ssh')
    @ssh_options.merge!(@host.ssh_properties)
    @ssh_options.merge!(options)
  end

  # HACK: A workaround for a race condition on a local resource (most likely known_hosts file).
  # @see "https://github.com/net-ssh/net-ssh/issues?utf8=%E2%9C%93&q=EBADF"
  # The work-around catches the "bad file handle" exception and simply retries after some time.
  def retry_connection?(error)
    return false unless error.inspect.to_s.include?('::EBADF:')

    if @retry_count > 2
      Tashi.log.error "Tried #{@retry_count} times to open SSH session, giving up... "
      return false
    end

    Tashi.log.debug("SSH failed (possible deadlock issue): retrying for the #{@retry_count} time...")
    sleep (1.5 * @retry_count).seconds
    @retry_count += 1
    true
  end
end
# rubocop:enable Metrics/ClassLength
