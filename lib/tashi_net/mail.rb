# frozen_string_literal: true

require 'net/pop'

# Wrapper around +Net::POP3+.
# Provides access to a mail server to *fetch* mails via pop3 protocol.
# There are currently no sending capabilities implemented. Feel free to extend this class!
#
# Author johannes.hoelken@hawk.de
#
class Mail
  # Default pop3 config
  DEFAULT_CONFIG = {
    port: 110,
    ssl: false
  }.freeze

  # Creates a valid File Name from a mails subject.
  # @param subject (String) the mails subject
  # @return (String) a valid file name
  def self.subject2filename(subject)
    filename = subject.dup.scrub('_').strip
    filename.gsub!(/[^0-9A-Za-z.\-]/, '_')
    "MAIL_#{filename}.txt"
  end

  # Constructor
  # @param address (String) The host name or IP address
  # @param user (String) The user name
  # @param pass (String) The users password
  # @param config (Hash) An optional options hash
  def initialize(address, user, pass, config: {})
    @user = user
    @pass = pass
    @address = address
    @config = DEFAULT_CONFIG.merge(config)

    if config[:ssl]
      Net::POP3.enable_ssl(OpenSSL::SSL::VERIFY_NONE)
    else
      Net::POP3.disable_ssl
    end
    @server = Net::POP3.new(address, config[:port])
  end

  # @return (String) Account name as user@server
  def account
    "#{@user}@#{@address}"
  end

  # Count the mails on the server
  # @return (Integer) the number of mails on the server
  def count
    count = with_session(&:n_mails)
    count = 0 if count.nil?
    count
  end

  # Deletes *all* mails on the server
  def clear
    with_session(&:delete_all)
  end

  # Wait for mails on the server
  # Note: This method will return immediately if there are old mails present. Use (@see clear) to
  # delete all old messages first
  # @param interval (Float) Time in sec to sleep between retries (default 15)
  # @param timeout (Integer) Time in sec to wait in total (default 180)
  # @raise (TestFailure) if the method times out.
  def wait_for_mails(interval: 15, timeout: 180)
    Tashi.log.info("Waiting up to #{timeout} seconds for mails on #{account} ...")
    start = Time.now
    while count.zero?
      Assert.flunk("Did not receive any mails on #{account} within #{timeout} seconds") if Time.now - start > timeout
      sleep(interval)
    end
    Tashi.log.info("Found #{count} mails for #{account}.")
  end

  # Retrieve mails from the server
  # @param delete (Boolean) Flag to determine if fetched messages shall be deleted (default: true)
  # @param working_dir (String) Path to store the mails as txt files if set (default: nil)
  # @return (Hash) The mails fetched as {subject => body} hash.
  def fetch_mails(delete: true, working_dir: nil)
    return {} if count.zero?

    mails = pop_mails(delete)
    save_to_disk(mails, working_dir) if working_dir.present?
    mails
  end

  private

  attr_reader :server

  # Wraps the stateful calls in a stateless protocol
  # So the user does not have to care about setup and teardown.
  def with_session
    server.start(@user, @pass)
    yield server
  ensure
    server.finish
  end

  def pop_mails(delete)
    mails = {}
    with_session do |srv|
      srv.mails.each { |mail| pop_mail(mails, mail, delete) }
    end
    mails
  end

  def save_to_disk(mails, dir)
    raise IOError, "#{dir} is not a directory" unless File.directory?(dir)

    mails.each do |subj, body|
      file = File.join(dir, Mail.subject2filename(subj))
      File.open(file, 'w') { |f| f.write body }
    end
  end

  def pop_mail(result, mail, delete)
    data = mail.pop
    subject = data.scan(/\r\nSubject: (.*)\r\n/).join('')
    result[subject] = data
    mail.delete if delete
  end
end
