# frozen_string_literal: true

require 'net/scp'

##
# Wrapper around +Net::SCP+.
#
# +Net::SCP+ is a pure-Ruby implementation of the SCP protocol
# and allows files and directory trees to be copied to and from a remote server.
#
# Author johannes.hoelken@hawk.de
#
class Scp
  ##
  # Constructor
  # @param host (Host/String) the host to connect to.
  #   If a string is given, it is assumed to be the identifier within the DepP.global.host registry.
  def initialize(host)
    @host = host.is_a?(String) ? DepP.global.host.find(host) : host
  end

  # upload to the host.
  #
  # Supported options
  #   :recursive  - the source parameter refers to a remote directory,
  #                which should be downloaded to a new directory named destination on the local machine.
  #   :preserve   - the atime and mtime of the file should be preserved.
  #   :verbose    - the process should result in verbose output on the server end (useful for debugging).
  #   :chunk_size - the size of each "chunk" that should be sent. Defaults to 2048.
  #                 Changing this value may improve throughput at the expense of decreasing interactivity.
  #   :ssh        - custom SSH properties. Uses the Host.ssh_properties as default.
  #
  # @param source (String) the source path. (The file/directory to upload)
  # @param destination (String) the destination path on the host. (The file/directory to upload to)
  # @param options (Hash) An optional options hash to specify the modes (see above) and set SSH options
  # @param user (String)  An optional user name to connect with. If not set the user from ssh_options will be used.
  #
  def upload(source, destination, user: nil, options: {})
    options = ssh_options(options)
    user = options[:ssh][:user] if user.nil?
    Tashi.log.info "SCP upload: '#{source}' => '#{user}@#{@host.address}:#{destination}'"
    connector.upload!(@host.address, user, source, destination, options, &method(:process_reporting))
  end

  # upload to the host.
  # @see upload instance method for details
  def self.upload(host, source, destination, options: {})
    Scp.new(host).upload(source, destination, options: options)
  end

  # download from the host.
  #
  # Supported options
  #   :recursive  - the source parameter refers to a remote directory,
  #                which should be downloaded to a new directory named destination on the local machine.
  #   :preserve   - the atime and mtime of the file should be preserved.
  #   :verbose    - the process should result in verbose output on the server end (useful for debugging).
  #   :ssh        - custom SSH properties. Uses the Host.ssh_properties as default.
  #
  # @param source (String) the source path on the host. (The file/directory to download)
  # @param destination (String) the destination path. (The file/directory to download to)
  # @param options (Hash) An optional options hash to specify the modes (see above) and set SSH options
  # @param user (String)  An optional user name to connect with. If not set the user from ssh_options will be used.
  #
  def download(source, destination, user: nil, options: {})
    options = ssh_options(options)
    user = options[:ssh][:user] if user.nil?
    Tashi.log.info "SCP download: '#{user}@#{@host.address}:#{source}' => '#{destination}'"
    connector.download!(@host.address, user, source, destination, options, &method(:process_reporting))
  end

  # download to the host.
  # @see download instance method for details
  def self.download(host, source, destination, options: {})
    Scp.new(host).download(source, destination, options: options)
  end

  # downloads multiple files in parallel from the host
  #
  # Supported options
  #   :recursive  - the source parameter refers to a remote directory,
  #                which should be downloaded to a new directory named destination on the local machine.
  #   :preserve   - the atime and mtime of the file should be preserved.
  #   :verbose    - the process should result in verbose output on the server end (useful for debugging).
  #   :chunk_size - the size of each "chunk" that should be sent. Defaults to 2048.
  #                 Changing this value may improve throughput at the expense of decreasing interactivity.
  #   :ssh        - custom SSH properties. Uses the Host.ssh_properties as default.
  #
  # @param list (Array) An Array of {source: "path", destination: "path"} Hashes
  # @param options (Hash) An optional options hash to specify the modes (see above) and set SSH options
  # @param user (String)  An optional user name to connect with. If not set the user from ssh_options will be used.
  #
  def parallel_download(list, options: {}, user: nil)
    options = ssh_options(options)
    user = options[:ssh][:user] if user.nil?
    Tashi.log.info "SCP download #{list.size} files in parallel as '#{user}@#{@host.address}'"
    connector.start(@host.address, user, options) { |scp| start_threads(scp, list, options) }
  end

  # downloads multiple files in parallel
  # @see parallel_downloads instance method for details
  def self.parallel_download(host, list, options: {})
    Scp.new(host).parallel_download(list, options: options)
  end

  private

  def start_threads(scp, list, options)
    threads =
      list.each_with_object([]) do |dl, thrds|
        thrds << scp.download(dl[:source], dl[:destination], options, &method(:process_reporting))
      end
    threads.each(&:wait)
  end

  def ssh_options(options)
    ssh_options = @host.ssh_properties.dup
    options[:ssh] = ssh_options.merge(options[:ssh] ||= {})
    options.keys_as_symbols(recursive: true)
  end

  ##
  # Logs the SCP progress
  def process_reporting(_channel, name, sent, total)
    current_progress = (sent.to_f / total * 100).to_i
    return unless (current_progress % 5).zero? && last_progress[name] != current_progress

    last_progress[name] = current_progress
    status = current_progress == 100 ? "\t [Done]" : ''
    Tashi.log.info "#{name}: \t #{current_progress}% of #{total} #{status}"
  rescue StandardError
    Tashi.log.debug "#{name}: Illegal progress value: #{sent} of #{total}"
  end

  def connector
    Net::SCP
  end

  def last_progress
    @last_progress ||= {}
  end
end
