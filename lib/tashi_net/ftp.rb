# frozen_string_literal: true

require 'net/ftp'

# Wrapper around +Net::FTP+.
#
# +Net::FTP+ is a pure-Ruby implementation of the FTP protocol and allows file operations as copied
# to and from a remote server, rename on remote, etc.
#
# If you open a FTP session in your test procedure the tashi-framework will automatically close it
# after the procedure is finished. Ensure to +#connect+ your new +Ftp+ instances. However, there is
# also a session-scoped method that will connect automatically and close the used session immediately
# after usage:
#
#    Ftp.with_session(my_host) do |ftp|
#      ftp.download('foo.txt', 'temp/bar.txt')
#      ftp.delete('foo.txt')
#    end
#
# This facade only implements the basic needs. If you need the full power of +net-ftp+ the
# complete library is exposed via the +ftp+ attribute.
#
# Author johannes.hoelken@hawk.de
#
class Ftp
  # A global synchronisation lock
  GLOBAL_LOCK = Mutex.new

  # Default FTP config. Available options
  #  - :port The FTP port (default: 21)
  #  - :passive The FTP mode (default: false)
  #  - :open_timeout The seconds to wait for opening the connection (default: 10)
  #  - :read_timeout The seconds to wait for interaction (default: 60)
  #  - :debug_mode Toggles verbosity (default: false)
  DEFAULT_CONFIG = {
    port: 21,
    passive: false,
    open_timeout: 10,
    read_timeout: 60,
    debug_mode: false
  }.freeze

  attr_reader :ftp

  ##
  # Open a FTP session on the given host.
  # Method takes a block with has the session object as argument.
  # Simple usage is
  #
  #    Ftp.with_session(my_host) do |ftp|
  #      ftp.download('foo.txt', 'temp/bar.txt')
  #      ftp.delete('foo.txt')
  #    end
  #
  # ensures the session is closed after the block.
  #
  # @param host (Host) the host to connect to.
  # @param config (Hash) an optional options hash.
  def self.with_session(host, config: {})
    session = Ftp.new(host, config: config).connect
    yield session
  ensure
    session.close
  end

  ##
  # This close hook will be called after each test procedure to close all remaining
  # SSH sessions that have been opened within the current thread.
  def self.close_hook
    Tashi::ThreadRegistry.call_all(ftp_sessions: :close)
    Tashi::ThreadRegistry.clear(:ftp_sessions)
  end

  # Constructor
  # @param host (Host) the host to connect to (can also refer to a proxy)
  # @param config (Hash) an optional options hash. (@see DEFAULT_CONFIG for available options)
  def initialize(host, config: {})
    @host = host.is_a?(String) ? DepP.global.host.find(host) : host
    @config = DEFAULT_CONFIG.merge(config)
    @ftp = nil
    @user = nil
  end

  # Connect to the host via FTP
  # @param user (String) the ftp-user, default: 'anonymous'
  # @param password (String) the password of the user, default: none
  # @return self
  def connect(user: 'anonymous', password: nil)
    return self if connected?

    configure_connection
    login(user, password)
    self
  end

  # Close the connection
  def close
    return unless connected?

    ftp.close
    print_status
  end

  # Check if we are connected
  def connected?
    !ftp.nil? && !ftp.closed?
  end

  # list files
  # @param dir (String) remote dirname to list
  # @return (Array) filenames in the remote directory
  def list(dir)
    check_status!
    ftp.nlst(dir)
  end

  # Download file +remote+ in binary mode, storing the result in +target+.
  # If +target+ is nil, method returns retrieved data directly.
  # @param remote (String) the remote file to download
  # @param target (String) the target to download to
  def download(remote, target)
    check_status!
    Tashi.log.info { "Downloading #{remote} from #{hostname}..." }
    ftp.getbinaryfile(remote, target)
  end

  # Transfers +local+ file to the server in binary mode, storing the result in +target+.
  # @param local (String) the file to upload
  # @param target (String) the target to upload to, default: basename of local
  def upload(local, target = File.basename(local))
    check_status!
    Tashi.log.info { "Uploading #{local} to #{hostname}/#{target}..." }
    ftp.putbinaryfile(local, target)
  end

  # Renames a file on the server.
  # @param old_name (String) the current file name
  # @param new_name (String) the new file name
  def rename(old_name, new_name)
    check_status!
    Tashi.log.info { "Renaming #{old_name} to #{new_name} on #{hostname}" }
    ftp.rename(old_name, new_name)
  end

  # Renames a file on the server.
  # @param remote (String) the file to delete
  def delete(remote)
    check_status!
    Tashi.log.info { "Deleting #{remote} on #{hostname}" }
    ftp.delete(remote)
  end

  # String representation of the FTP connection
  def to_s
    string = "FTP on #{hostname}"
    string += " as user '#{user}'" unless user.nil?
    string += " is connected: #{connected?}"
    string
  end

  private

  attr_reader :config, :host, :user

  def configure_connection
    @ftp = Net::FTP.new
    @ftp.passive = config[:passive]
    @ftp.open_timeout = config[:open_timeout]
    @ftp.read_timeout = config[:read_timeout]
    @ftp.debug_mode = config[:debug_mode]
  end

  def login(user, password)
    @user = user
    @ftp.connect(host.address, config[:port])
    @ftp.login(user, password)
    Tashi::ThreadRegistry.register(ftp_sessions: self)
    print_status
  end

  def hostname
    "ftp://#{host.address}:#{config[:port]}"
  end

  def check_status!
    raise IllegalStateError, "FTP connection #{hostname} is not open" unless connected?
  end

  def print_status
    Tashi.log.info(to_s)
  end
end
