# frozen_string_literal: true

# Extension of Assertion module with net specific assertions
# Author johannes.hoelken@hawk.de
module Assert
  # Assertions for SSH interactions
  module SSH
    # Check if the command was successful (exit code == 0)
    # @param (Ssh::Command) the command to check
    # @raise (TestFailure) if the commands exit code is non-zero
    def self.success(command)
      return if command.succeeded?

      Tashi.log.warn("Command #{command.instruction} not successful. ExitCode: #{command.exit_code}")
      Tashi.log.warn("[SshErr]: #{command.std_err}")
      Assert.flunk("SSH Command #{command.instruction} failed with return code #{command.exit_code}.")
    end
  end
end
