# frozen_string_literal: true

# Namespace
class Ssh
  ##
  # Utility class to provide callback implementations for net-ssh callback methods.
  # Author johannes.hoelken@hawk.de
  class Callbacks
    attr_reader :channel, :out_wait_condition, :out_wait_expressions, :converter, :filter

    # Constructor
    # @param channel (Ssh::Channel) the channel the callbacks are for
    # @param converter (Object) data_converter instance, must respond_to 'convert'
    # @param filter (Object) response_filter instance, must respond_to 'filter' and 'input_requests'
    def initialize(channel, converter, filter)
      @channel = channel
      @converter = converter
      @filter = filter

      @out_wait_condition = ConditionVariable.new
      @out_wait_expressions = []
      @last_out = ''
      @last_err = ''
    end

    def on_data(data)
      current_out = @converter.convert(data)
      Tashi.log.info { "[SSH-Channel] [StdOut]: #{current_out}" }
      response = response_from_out(current_out)
      @channel.public_send(response.first, response.last) unless response.nil?
    end

    def on_error(data)
      current_out = @converter.convert(data)
      Tashi.log.warn { "[SSH-Channel] [StdErr]: #{current_out}" }

      @channel.synchronized do
        @channel.idle_since = Time.now
        @last_err += current_out.to_s
        @channel.command.std_err += current_out.to_s

        signal_wait_statement_match(@last_err)
      end
    end

    def on_process
      @channel.timed_out = false
      @channel.synchronized do
        return if Time.now - @channel.idle_since < @channel.idle_timeout

        message = "[SSH-Channel] [StdErr]: Command '#{@channel.command.instruction}'"
        message += " Timed out after #{@channel.idle_timeout} seconds."
        Tashi.log.warn(message)
        # Set idle time to prevent printing this twice!
        @channel.idle_since = Time.now
        @channel.timed_out = true
      end
      @channel.close(-10)
    end

    def on_exit_status(data)
      @channel.command.exit_code = data.read_long
    end

    def on_exit_signal(data)
      # If an ongoing session is closed locally, the remote host will send us a HUP, which we don't need to report
      return if @channel.local_closed?

      message = "[SSH-Channel] [SshErr]: SSH command '#{@channel.command.instruction}'"
      message += " terminated with message '#{data.read_string}'"
      Tashi.log.error(message)
      @channel.command.exit_code = -11
    end

    def wait_for_condition(condition)
      @channel.synchronized do
        wait_for_initialization if condition.blank?
        wait_for(condition)
      end
    end

    protected

    # Take care of the wait statements
    # The input data is part of a stream, hence the wait statement match must be applied on the complete data.
    # Will broadcast on the (ConditionVariable) once any +out_wait_condition+ is met by the data,
    # and thereby release the waiting user thread.
    def signal_wait_statement_match(data)
      @out_wait_expressions.each do |wait_for|
        next unless wait_for.match(data.to_s)

        @out_wait_condition.broadcast
        break
      end
    end

    # Match the given out against the expected responses from the command.
    # If one matches, return the pre-configured answers to the corresponding channel method.
    # Note: If the response is not a String it is considered an exit code and the channel is terminated!
    def determine_response(out)
      @channel.command.data_responses.each do |selector, response|
        next unless selector.match(out)

        Tashi.log.debug { "[SSH-Channel] Response matches selector #{selector}." }
        return [:enter, response] if response.is_a? String

        Tashi.log.debug { "[SSH-Channel] Got expected response '#{out}'. Abort SSH Command '#{@command.instruction}'." }
        return [:close, response]
      end
      nil
    end

    private

    def wait_for(condition)
      return ['', ''] if condition.blank?

      @out_wait_expressions = condition
      out_wait_condition.wait(@channel.in_out_lock)
      result = [@last_out.dup, @last_err.dup]
      @last_out = ''
      @last_err = ''
      result
    end

    # In Multithreading the channel may not be ready, so we wait a few seconds for initialization.
    # rubocop:disable Style/WhileUntilModifier
    # disable as otherwise Style/NestedModifier would be violated
    def wait_for_initialization
      start = Time.now
      until @channel.ready?
        raise 'Unable to open SSH Channel after 10 seconds.' if Time.now - start > 10
      end
    end
    # rubocop:enable Style/WhileUntilModifier

    def response_from_out(current_out)
      @channel.synchronized do
        @channel.idle_since = Time.now

        # Take care of simple data responses (without any logic)
        # Compare complete output, since the data returned by SSH server is just part of a stream
        response = determine_response(@last_out + current_out.to_s)
        if response.blank?
          @last_out += current_out.to_s
          @channel.command.std_out += current_out.to_s
        end
        signal_wait_statement_match(@last_out)
        return response
      end
    end
  end
end
