# frozen_string_literal: true

# Namespace
class Ssh
  ##
  # Class to convert linux ssh return strings in UTF-8 save stings.
  #
  # Author johannes.hoelken@hawk.de
  class LinuxDataConverter
    # The following config options are provided:
    #   * +:filter_ansi+ Filter ANSI color codes and cursor position commands
    #   * +:replace_xterm_escapes+ Replace/filter XTERM title, tabulator, clear so that the result is readable UTF-8
    DEFAULT_CONFIG = { filter_ansi: true, replace_xterm_escapes: false }.freeze

    # Per default we replace ANSI color codes and empty ANSI
    ANSI_REPLACEMENTS = { /\e\[[0-9;]+m/ => '', /\e\[m/ => '' }.freeze

    # Per default xterm replacement is disabled.
    XTERM_REPLACEMENTS = {
      /\e\]0;[^\a;]+\a/ => '', # xtitle
      /\e\[\?[0-9;]+[a-z]/ => '', # terminal control commands
      /\e=/ => '', # Terminal variables
      /\e\[H/ => "\n", # Home
      /\e\[2J/ => '', # Clear
      /\e\(B/ => '  ', # Indent
      /\e\[K/ => "\t" # Tab
    }.freeze

    attr_reader :config, :replacements

    ##
    # Constructor
    # @param config (Hash) an optional config hash to enable/disable replacements
    def initialize(config = {})
      @config = DEFAULT_CONFIG.merge(config)
      @replacements = {}
      @replacements = @replacements.merge(ANSI_REPLACEMENTS) if @config[:filter_ansi]
      @replacements = @replacements.merge(XTERM_REPLACEMENTS) if @config[:replace_xterm_escapes]
    end

    ##
    # Converts a given input string to utf8.
    # Also replaces special characters based on configuration
    # @param input (Object) the input to convert. Will only be touched if input is a (String).
    # @return the converted input.
    def convert(input)
      return input unless input.is_a? String

      replace_chars(input.scrub('?').strip)
    end

    private

    def replace_chars(input_data)
      lines =
        input_data.lines("\n").each_with_object([]) do |line, converted_lines|
          replacements.each do |item_regexp, item_replace|
            line.gsub!(item_regexp, item_replace)
          end
          converted_lines << line
        end
      lines.join("\n")
    end
  end
end
