# frozen_string_literal: true

class Ssh
  # Sending interface for SSH::Channel
  # Should not be used individually but only via Ssh::Channel
  #
  # Author johannes.hoelken@hawk.de
  class Sender
    attr_reader :channel, :callbacks
    attr_accessor :default_wait

    ##
    # Constructor
    # @param channel (SSH::Channel) the net-ssh channel to send data on
    # @param callbacks (Ssh::Callbacks) the configured callbacks
    def initialize(channel, callbacks)
      @channel = channel
      @callbacks = callbacks
      @default_wait = {}
    end

    ##
    # Executes the command on the channel
    # @param cmd (Ssh::Command) the command to execute
    # @param pty (Boolean) flag to determine if a (pseudo) tty shall be obtained
    def exec(cmd, pty: false)
      set_callbacks
      acquire_pty if pty
      exec_command(cmd)
    end

    # Send data to the open channel.
    # Use this to send replies on pty channels. Open the Channel with (@see exec) first.
    # @param data (String) the data to send
    # @param wait_for (Array) the condition(s) (RegEx) to wait for.
    def enter(data, wait_for = nil)
      Tashi.log.debug("[SSH-Channel]: Sending '#{data}'")
      channel.send_data("#{data}\n")
      out, err = callbacks.wait_for_condition(wait_for.present? ? wait_for : [])
      [callbacks.filter.filter(data, out), err]
    end

    # Close the underlying channel
    def close
      channel.close
    end

    # Check if the channel was closed from the local side.
    def local_closed?
      channel.local_closed?
    end

    # Check if the (SSH::Channel) is active
    def inactive?
      !channel.active? || channel.closing?
    end

    private

    # rubocop:disable Metrics/AbcSize
    def set_callbacks
      channel.on_data { |_ch, data| callbacks.on_data(data) }
      channel.on_extended_data { |_ch, data| callbacks.on_error(data) }
      channel.on_process { |_ch| callbacks.on_process }
      channel.on_request('exit-status') { |_ch, data| callbacks.on_exit_status(data) }
      channel.on_request('exit-signal') { |_ch, data| callbacks.on_exit_signal(data) }
    end
    # rubocop:enable Metrics/AbcSize

    def acquire_pty
      channel.request_pty do |_ch, success|
        raise Net::SSH::Exception, 'Unable to obtain pty!' unless success

        Tashi.log.debug '[SSH-Channel]: pty successfully obtained'
      end
    end

    def exec_command(cmd)
      Tashi.log.debug("[SSH]: Executing '#{cmd.instruction}'")
      channel.exec(cmd.instruction) do |_ch, success|
        raise "Couldn't execute command" unless success

        cmd.exit_code = 0
      end
    end
  end
end
