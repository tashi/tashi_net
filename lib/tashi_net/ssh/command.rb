# frozen_string_literal: true

# Namespace
class Ssh
  ##
  # Ssh Command / Result wrapper
  #
  # Author johannes.hoelken@hawk.de
  class Command
    attr_accessor :std_out, :std_err, :aborted, :data_responses
    attr_reader :instruction, :exit_code

    # Constructor
    def initialize(command, data_responses = {})
      @instruction = command
      @data_responses = data_responses
      @aborted = false
      @std_out = ''
      @std_err = ''
      @exit_code = -1
    end

    # String representation of result
    def to_s
      string = "SSH Command '#{instruction}' resulted in:\n"
      string += "--- STD_OUT ---\n#{utf8_out}"
      string += "--- STD_ERR ---\n#{utf8_err}" if std_err.present?
      string
    end

    # Get +STD OUT* UTF8 safe
    # @return (String) utf-8 encoded std_out string.
    def utf8_out(_encoding_options = {})
      @std_err = std_out[1..-1] if std_out[0] == "\n"
      to_utf8(std_out)
    end

    # Get +STD ERR* UTF8 safe
    # @return (String) utf-8 encoded std_err string.
    def utf8_err(_encoding_options = {})
      @std_err = std_err[1..-1] if std_err[0] == "\n"
      to_utf8(std_err)
    end

    # Check if the command given succeeded.
    # @param expected_code (Integer) The expected return code, default: 0
    # @return (Boolean) true iff the exit code equals the expected code
    def succeeded?(expected_code: 0)
      exit_code == expected_code
    end

    # Check if command was aborted
    def aborted?
      aborted
    end

    # set the exit code
    def exit_code=(code)
      return if aborted?

      @exit_code = code
    end

    # Set the result in a fast way by passing an (Array) where the first
    # value is assumed to be the STD_OUT and the last the STD_ERR string.
    def result(out_arr)
      @std_out += out_arr.first
      @std_err += out_arr.last
    end

    private

    ##
    # Enforces utf-8 encoding.
    # @param string (String) input string to be processed
    # @return (String) utf-8 encoded string.
    def to_utf8(string)
      string.scrub('?').strip
    end
  end
end
