# frozen_string_literal: true

# Namespace
class Ssh
  # A channel in SSH is an active input/output stream on an active SSH session.
  # A session can host multiple channels.
  # The life cycle of a SSH channel corresponds to the execution of the command.
  #
  # This class provides utility function to interact with the command responses.
  # However, the user should not use or see this directly but rather get the feeling of a 'real' ssh shell
  # where channels are also transparent for the user.
  # This class is accessed via (@see Ssh.invoke) or (@see Ssh.with_channel).
  #
  # Author johannes.hoelken@hawk.de
  #
  # rubocop:disable Metrics/ClassLength
  class Channel
    attr_reader :command, :idle_timeout, :in_out_lock
    attr_accessor :idle_since, :timed_out

    # rubocop:disable Metrics/MethodLength
    def initialize(command, timeout, os, data_responses)
      raise IllegalArgumentError, "Unsupported target OS '#{os}'!" unless %w[cygwin mingw linux].include?(os)

      @sender = nil
      @timed_out = false
      @idle_since = Time.now
      @in_out_lock = Mutex.new

      # Potentially OS specific. On extension of supported OSes ensure to verify
      @callbacks = Ssh::Callbacks.new(
        self,
        Ssh::LinuxDataConverter.new(replace_xterm_escapes: true),
        Ssh::LinuxResponseFilter.new(filter_shell_input_request: true)
      )
      @exit_code_request = 'echo $?'

      @command = Ssh::Command.new(command, data_responses)
      @idle_timeout = timeout
    end
    # rubocop:enable Metrics/MethodLength

    # Check if the channel was closed from the local side.
    def local_closed?
      @sender&.local_closed?
    end

    # Check if the (SSH::Channel) is ready
    def ready?
      !@sender.nil?
    end

    # Check if the (SSH::Channel) is active
    def inactive?
      !ready? || @sender&.inactive?
    end

    # Synchronize the channel via in/out lock
    def synchronized
      @in_out_lock.synchronize { yield }
    end

    # Executes the given channel's command
    # @param session (Net::SSH::Session) the session to execute the command in
    # @param require_pty (Boolean) flag to determine if a pty shell shall be used
    # @param loop_interval (Integer) number of loops to wait for the session to be not busy anymore. Default: 10
    # @param default_wait (Array) Regexes to wait for. Default: input prompts for the current OS
    # @return (Ssh::Command) the command data item with the recorded results
    def exec(session, require_pty, loop_interval: 10, default_wait: nil)
      with_error_handling do
        session.open_channel do |channel|
          @sender = Ssh::Sender.new(channel, @callbacks)
          @sender.default_wait = default_wait.nil? ? @callbacks.filter.input_requests : default_wait
          @sender.exec(command, pty: require_pty)
        end

        session.loop(loop_interval) { !command.aborted? && session.busy? }
        session.shutdown! if @timed_out
      end
      @command
    end

    # Execute the given channel's command in dedicated thread and returns
    # when wait_for condition is met
    # @param session (Net::SSH::Session) the session to execute the command in
    # @param require_pty (Boolean) flag to determine if a pty shell shall be used
    # @param wait_for (Array) Array of Regexes to be matched to satisfy the command
    # @return (Thread) the thread the command is executed in
    def exec!(session, require_pty, wait_for = nil)
      thread = Thread.new { threaded_execution(session, require_pty, wait_for) }
      @callbacks.wait_for_condition(wait_for.nil? ? @callbacks.filter.input_requests : wait_for)
      thread
    end

    # Method mimics the (@see Ssh#invoke) method to allow
    # coding with the same syntax regardless if the user uses a Session or a Channel
    # Will send the actual command and an exit_code_request (e.g. 'echo "$?"') to obtain
    # the commands result
    # @param cmd (String) the command to execute
    # @param check_success (Boolean) If true a non-zero exit code will fail the test step.
    # @param wait_for (Array) Array of Regexes to be matched to satisfy the command
    # @return (Ssh::Command) the command data item with the recorded results
    def invoke(cmd, check_success: false, wait_for: nil)
      command = Ssh::Command.new(cmd)
      if inactive?
        Tashi.log.info "Channel is inactive. Couldn't send '#{cmd}'...'"
        Assert.flunk("Channel is inactive. Couldn't send '#{cmd}'...'") if check_success
        return command
      end

      command.result(enter(cmd, wait_for.present? ? wait_for : @callbacks.out_wait_expressions))
      command.exit_code = exit_code
      Assert::SSH.success(command) if check_success
      command
    end

    # Get exit code of previous command
    def exit_code(wait_for = [/^[0-9]+$/])
      return -101 if inactive?

      with_other_timeout(10) do
        out, _err = enter(@exit_code_request, wait_for)
        extract_code(out)
      end
    end

    # Send data to the channel.
    # @param data (String) the data to send
    # @param wait_for (Array) the condition(s) (RegEx) to wait for.
    def enter(data, wait_for = nil)
      @sender.enter(data, wait_for)
    end

    # Aborts the connection with the remote host
    # E.g. the Ssh::Channel waits for the termination of the initial command or a timeout.
    # So in case you opened for example a csh on the remote host, do not use this method, but
    # do a enter("exit") instead.
    # If no exit code is given, the exit code is not modified
    def close(exit_code = nil)
      synchronized do
        @command.exit_code = exit_code unless exit_code.nil?
        @command.aborted = true
        @sender&.close
      end
    end

    private

    def threaded_execution(session, require_pty, wait_for)
      Thread.current[:name] = 'ssh_channel'
      exec(session, require_pty, loop_interval: 1, default_wait: wait_for)
    rescue StandardError => e
      Tashi.exception(e, message: 'Internal error during SSH channel callback handling')
      close(-99)
    ensure
      # Assure the main thread does not wait forever when we have an internal error
      @callbacks.out_wait_condition&.broadcast
    end

    def with_other_timeout(sec)
      original_timeout = @idle_timeout.dup
      @idle_timeout = sec
      yield
    ensure
      @idle_timeout = original_timeout
    end

    def extract_code(string)
      any_number = /^\d+$/
      string.lines.reverse.each do |line|
        next unless any_number.match(line)

        return Integer(line.strip)
      end
      Tashi.log.error("No valid exit code found in return string:\n#{string}")
      -1
    rescue ArgumentError
      Tashi.log.error("Unexpected exit code return string:\n#{string}")
      -1
    end

    def with_error_handling
      yield
    rescue IOError => e
      handle_io_error(e)
    rescue StandardError => e
      handle_std_error(e)
    end

    def handle_io_error(err)
      Tashi.log.info("[SSH-Channel] [SSH]: #{err.class.name} #{err.message}")
      close(-2)
    end

    def handle_std_error(err)
      if ready? && !@command.aborted?
        Tashi.exception(err, message: '[SSH-Channel] [SshErr]: Exception during SSH interaction.')
        close(-3)
      else
        Tashi.log.debug("[SSH-Channel] Error that can be most likely ignored: #{err.class.name} #{err.message}")
      end
    end
  end
  # rubocop:enable Metrics/ClassLength
end
