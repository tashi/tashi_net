# frozen_string_literal: true

# Namespace
class Ssh
  # The LinuxResponseFilter takes data from (NET::SSH) +on_data+ call-back
  # (after pre-processing)and perform filtering.
  # This filter supports only Linux SSH prompts (hence the name).
  #
  # Author: johannes.hoelken@hawk.de
  class LinuxResponseFilter
    # Regexes to filter for shell input request prompts
    SHELL_INPUT_REQUESTS = [/(.*\$\s?)$/m, /(.*\#\s?)$/m, /(.*\>\s?)$/m].freeze

    # The following options are provided:
    #   * +:filter_send_echo+ Filter first line in case it is equal to the data send
    #   * +:filter_shell_input_request+ Filter last line in case it matches SHELL_INPUT_REQUESTS
    DEFAULT_CONFIG = { filter_send_echo: true, filter_shell_input_request: false }.freeze

    ##
    # Constructor
    # @param config (Hash) an optional config hash to enable/disable replacements
    def initialize(config = {})
      @config = DEFAULT_CONFIG.merge(config)
    end

    # Extract the real response to a request from additional data like echoing the
    # request or displaying a command prompt.
    # @param send_data (String) the input data send, i.e. a command
    # @param response_data (String) the response received from net-ssh
    # @return (String) the filtered response
    def filter(send_data, response_data)
      result = response_data
      result = filter_send_echo(send_data, result) if @config[:filter_send_echo]
      result = filter_shell_input_request(result) if @config[:filter_shell_input_request]
      result
    end

    # @return (Array) of Regexes to filter for shell input request prompts
    def input_requests
      SHELL_INPUT_REQUESTS.dup
    end

    private

    def filter_send_echo(send_data, response_data)
      return response_data if send_data.nil?
      return response_data unless send_data.is_a?(String)
      return response_data unless response_data.start_with?(send_data)

      response_data[send_data.length..-1]
    end

    def filter_shell_input_request(response_data)
      last_line = response_data.lines.last
      return response_data if last_line.blank?

      match_suffix = first_match(last_line, input_requests)
      return response_data if match_suffix.nil?

      data = match_suffix[-1]
      return '' if data.present? && data.length == response_data.length

      response_data[0..-(data.length + 1)].chomp
    end

    def first_match(string, regexs)
      regexs.each do |regex|
        match = regex.match(string)
        return match unless match.nil?
      end
      nil
    end
  end
end
