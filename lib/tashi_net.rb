# frozen_string_literal: true

require 'tashi'

require 'tashi_net/version'
require 'tashi_net/ftp'
require 'tashi_net/mail'
require 'tashi_net/scp'
require 'tashi_net/ssh'

# Extension of assertion module
require 'tashi_net/assert'

# bug workaround
require 'extensions/ssh_jruby_fix'

# TashiNat is the Network extension of TASHI
# It does not really provide its own namespace but will use TASHI.
#
# Author johannes.hoelken@hawk.de
module TashiNet
  DepP.global.closeable_helper << Ftp
  DepP.global.closeable_helper << Ssh
end
